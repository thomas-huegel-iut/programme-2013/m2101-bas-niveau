public class MultipleAllocations {
	private static final int SIZE = 100000000;

	private static void measureTime(Runnable f) {
		long time = System.nanoTime();
		f.run();
		System.out.println("Temps total : " + (System.nanoTime() - time) * 1e-9);
	}

	private static void makeOneBigAllocation() {
		int[] p = new int[MultipleAllocations.SIZE];
	}

	public static void main(String[] args) {
		MultipleAllocations.measureTime(MultipleAllocations::makeOneBigAllocation);
	}
}
